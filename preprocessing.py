import pandas as pd


def setAzimuth(value):
    if value <= 1800.0:
        return value / 1800.0
    else:
        return 1 - ((value / 1800.0) - 1)


def normalizeCoordinates(data: pd.DataFrame, scaler: int, normalizeOtherData: bool) -> pd.DataFrame:
    """
    //TODO opis
    :param data:
    :param scaler:
    :param normalizeOtherData:
    :return:
    """
    minX = data["X-coordinate"].min()
    maxX = data["X-coordinate"].max()
    minY = data["Y-coordinate"].min()
    maxY = data["Y-coordinate"].max()

    data["X-coordinate"] = ((data["X-coordinate"] - minX) / (maxX - minX)) * scaler
    data["Y-coordinate"] = ((data["Y-coordinate"] - minY) / (maxY - minY)) * scaler

    if normalizeOtherData:
        #data["azimuth"] = data["azimuth"] / 3600.0
        data['azimuth'] = data['azimuth'].apply(lambda x: setAzimuth(x))
        data["altitude"] = data["altitude"] / 900.0
        data["pressure"] = data["pressure"] / 1023.0

    return data
