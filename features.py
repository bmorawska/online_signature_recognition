import math

import pandas as pd


def signatureTotalDuration(data: pd.DataFrame) -> int:
    """
    Liczba milisekund od rozpoczęcia podpisu do zakończenia.
    :param data: dane o podpisie
    :return: liczba milisekund
    """
    lastIdx = len(data["timestamp"]) - 1
    return data["timestamp"][lastIdx]


def numberOfPenUps(data: pd.DataFrame) -> int:
    """
    Liczy ile razy pisaczek został podniesiony.

    Podniesienie pisaczka występuje kiedy 0 zmienia się na 1,
    czyli można zapisać, że jak mamy serię danych
    0, 0, 0, 0, 1, 1, 1, 1, 0, 0, to mamy 1 podniesienie
    jeśli odejmiemy wartość od jej następnej wartości to wyniki
    mogą być 0 (0 - 0 i 1 - 1), 1 (opuszczenie pisaczka), -1 (podniesienie pisaczka)
    :param data: dane o podpisie
    :return: liczba podniesień pisaczka
    """

    penUps = data["buttonStatus"].diff()
    numberOfPenUps = penUps[penUps == -1].count()

    return numberOfPenUps


def meanAndStdDev(data: pd.DataFrame) -> tuple:
    """
    Zwraca średnią oraz odchylenie standardowe otrzymanego zestawu danych
    :param data: dane o podpisie
    :return: średnia oraz odchylenie standardowe
    """
    meanOfData = data.mean()
    stdDevOfData = data.std()

    return meanOfData, stdDevOfData


def calculateAverageVelocity(data: pd.DataFrame, time: float) -> float:
    """
    Oblicza średnią prędkość liczoną jako całkowita droga w znormalizowanym układzie współrzędnych
    dzielona przez całkowity czas pisania
    :param data: dane o podpisie
    :param time: całkowity czas składania podpisu (w sekundach)
    :return: średnia prędkość pisania
    """
    avgSum = 0

    dataLength = len(data["X-coordinate"])
    for i in range(1, dataLength):
        avgSum += math.sqrt((data["X-coordinate"][i] - data["X-coordinate"][i - 1]) * (
                data["X-coordinate"][i] - data["X-coordinate"][i - 1]) +
                            (data["Y-coordinate"][i] - data["Y-coordinate"][i - 1]) * (
                                    data["Y-coordinate"][i] - data["Y-coordinate"][i - 1]))

    avgSum = avgSum / time

    return avgSum


def calculateVelocityForAxis(data: pd.DataFrame) -> pd.DataFrame:
    """
    Oblicza składowe prędkości wzdłuż osi X oraz Y
    :param data: dane o podpisie
    :return: dane składowych prędkości oraz złożenia tych prędkości
    """
    xVelocity = [0.0]
    yVelocity = [0.0]

    dataLen = len(data["X-coordinate"])

    for i in range(1, dataLen):
        diffTime = data["timestamp"][i] - data["timestamp"][i - 1]
        if diffTime == 0:
            diffTime = 10
        xVel = (data["X-coordinate"][i] - data["X-coordinate"][i - 1]) // (diffTime * 0.001)
        yVel = (data["Y-coordinate"][i] - data["Y-coordinate"][i - 1]) // (diffTime * 0.001)
        xVelocity.append(xVel)
        yVelocity.append(yVel)

    temp = {'X-velocity': xVelocity, 'Y-velocity': yVelocity}
    df = pd.DataFrame(temp)
    return df


def assemblyFeatureVectorForData(data: pd.DataFrame) -> list:
    """
    Składa wszystkie cechy w pojedynczy wektor
    :param data: dane o podpisie
    :return: wektor cech dla konkretnych danych
    """
    time = signatureTotalDuration(data) * 0.001
    penUps = numberOfPenUps(data)
    xCoordMean, xCoordStd = meanAndStdDev(data["X-coordinate"])
    yCoordMean, yCoordStd = meanAndStdDev(data["Y-coordinate"])
    azimuthMean, azimuthStd = meanAndStdDev(data["azimuth"])
    altitudeMean, altitudeStd = meanAndStdDev(data["altitude"])
    pressureMean, pressureStd = meanAndStdDev(data["pressure"])
    averageVelocity = calculateAverageVelocity(data, time)

    velocityData = calculateVelocityForAxis(data)
    xVelocityMean, xVelocityStd = meanAndStdDev(velocityData["X-velocity"])
    yVelocityMean, yVelocityStd = meanAndStdDev(velocityData["Y-velocity"])

    featureVector = [time, penUps, averageVelocity, xCoordMean, xCoordStd, yCoordMean, yCoordStd,
                     azimuthMean, azimuthStd, altitudeMean, altitudeStd, pressureMean, pressureStd,
                     xVelocityMean, xVelocityStd, yVelocityMean, yVelocityStd]
    """
    print("Czas: {}".format(time))
    print("Liczba podniesień długopisu: {}".format(penUps))
    print("Średnia prędkość pisania: {}".format(averageVelocity))
    print("Średnia oraz odchylenie standardowe współrzędnej X: {}, {}".format(xCoordMean, xCoordStd))
    print("Średnia oraz odchylenie standardowe współrzędnej Y: {}, {}".format(yCoordMean, yCoordStd))
    print("Średnia oraz odchylenie standardowe orientacji: {}, {}".format(azimuthMean, azimuthStd))
    print("Średnia oraz odchylenie standardowe pochylenia: {}, {}".format(altitudeMean, altitudeStd))
    print("Średnia oraz odchylenie standardowe nacisku: {}, {}".format(pressureMean, pressureStd))
    print("Średnia oraz odchylenie standardowe prędkości po osi X: {}, {}".format(xVelocityMean, xVelocityStd))
    print("Średnia oraz odchylenie standardowe prędkości po osi Y: {}, {}".format(yVelocityMean, yVelocityStd))
    """
    return featureVector

def assemblyFeatureVectorForData2(data: pd.DataFrame) -> list:
    """
    Składa wszystkie cechy w pojedynczy wektor
    :param data: dane o podpisie
    :return: wektor cech dla konkretnych danych
    """
    time = signatureTotalDuration(data) * 0.001
    penUps = numberOfPenUps(data)
    xCoordMean, xCoordStd = meanAndStdDev(data["X-coordinate"])
    yCoordMean, yCoordStd = meanAndStdDev(data["Y-coordinate"])
    azimuthMean, azimuthStd = meanAndStdDev(data["azimuth"])
    altitudeMean, altitudeStd = meanAndStdDev(data["altitude"])
    pressureMean, pressureStd = meanAndStdDev(data["pressure"])
    averageVelocity = calculateAverageVelocity(data, time)

    featureVector = [time, penUps, averageVelocity, xCoordMean, xCoordStd, yCoordMean, yCoordStd,
                     azimuthMean, azimuthStd, altitudeMean, altitudeStd, pressureMean, pressureStd]
    """
    print("Czas: {}".format(time))
    print("Liczba podniesień długopisu: {}".format(penUps))
    print("Średnia prędkość pisania: {}".format(averageVelocity))
    print("Średnia oraz odchylenie standardowe współrzędnej X: {}, {}".format(xCoordMean, xCoordStd))
    print("Średnia oraz odchylenie standardowe współrzędnej Y: {}, {}".format(yCoordMean, yCoordStd))
    print("Średnia oraz odchylenie standardowe orientacji: {}, {}".format(azimuthMean, azimuthStd))
    print("Średnia oraz odchylenie standardowe pochylenia: {}, {}".format(altitudeMean, altitudeStd))
    print("Średnia oraz odchylenie standardowe nacisku: {}, {}".format(pressureMean, pressureStd))
    print("Średnia oraz odchylenie standardowe prędkości po osi X: {}, {}".format(xVelocityMean, xVelocityStd))
    print("Średnia oraz odchylenie standardowe prędkości po osi Y: {}, {}".format(yVelocityMean, yVelocityStd))
    """
    return featureVector
