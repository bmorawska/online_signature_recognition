# Biometryczne wspomaganie interakcji człowiek-komputer
_Rozpoznawanie podpisu odręcznego_

**Autorzy:**

- Julia Zaborowska      234134

- Barbara Morawska      234096

- Andrzej Sasinowski    234118

Wydział Fizyki Technicznej, Informatyki i Matematyki Stosowanej

Politechnika Łódzka 

2019/2020 

## Wymagania 

- Python > 3.6

- Numpy (1.18.2) 

## Uruchomienie programu (użytkownik)
Aby uruchomic program należy w wejść do katalogu, w którym znajduje
się plik ``main.py``, a następnie wpisać komendę:

_Na systemie Windows_
```python
python main.py
```

_Na systemach Linux_
```python
python3 main.py
```

## Uruchomienie programu (developer)

#### Dodawanie wirtualnego środowiska w Pycharm

```
File -> 
Settings-> 
Project (nazwa projektu) -> 
Project interpreter -> 
Koło zębate po prawej stronie obok pola "Project Interpreter" -> 
Add -> 
New environment -> 
Ok
```

Następnie trzeba chwilkę poczekać i uruchomić terminal python na dole ekranu. W okno terminala należy wpisać:

```bash
pip install -r requirements.txt
```

co zainstaluje wymagane biblioteki.

Następnie należy uruchomić program poprzez wejście do katalogu, w którym znajduje się plik ``main.py``, a następnie wpisać komendę:

_Na systemie Windows_
```python
python main.py
```

_Na systemach Linux_
```python
python3 main.py
```

#### Aktualizacja pliku requirements.txt 
Po dodaniu biblioteki należy dodać ją też do pliku requirements.txt, w taki sposób:

```bash
pip freeze > requirements.txt
```
