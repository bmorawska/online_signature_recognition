import numpy as np
import pandas as pd

from features import assemblyFeatureVectorForData
from fileLoader import loadFile


def KNN(vector: list, N: int):

    featureVectorsForUser = []
    for user in range(1, 5):
        for signature in range(1, 5):
            data = loadFile(userID=user, signatureID=signature, drawPlot=False)
            featureVectorsForUser.append([user, signature, assemblyFeatureVectorForData(data)])

    newSignatureVector = vector
    distances = []
    for signature in range(1, len(featureVectorsForUser)):
        userSignature = np.array(featureVectorsForUser[signature][2])
        dist = np.linalg.norm(userSignature - newSignatureVector)
        distances.append([featureVectorsForUser[signature][0], featureVectorsForUser[signature][1], dist])

    distances = pd.DataFrame(distances, columns=['user', 'signature', 'vector'])
    distances = distances.sort_values(by=['vector'])

    mode = []
    while len(mode) != 1:
        mode = distances["user"].head(N).mode()
        N += 1

    userID = mode[0]
    # print("KNN")
    # print(distances.head(10))

    return userID


def KNN2(vector, df, thresholdForUser, N: int):

    newdf = df.copy()
    distances = []
    for index, row in df.iterrows():
        dbVector = row[4:]
        dist = np.linalg.norm(vector - dbVector)
        distances.append(dist)

    newdf['distance'] = distances

    newdf = newdf.sort_values(by='distance', ascending=True)
    #newdf = newdf.drop(newdf.index[0])

    top = []
    while len(top) != 1:
        top = newdf["user"].head(N).mode()
        N += 1

    #print("KNN")
    #print(newdf.head(5))

    userID = top[0]

    isTrueSignature = True
    #fistElementIndex = newdf.loc[newdf['user'] == userID].index[0]
    #print(fistElementIndex)
    closestDist = float(newdf['distance'].iloc[1])
    #print(closestDist)
    if closestDist > thresholdForUser:
        isTrueSignature = False

    return userID, isTrueSignature



"""
newSignature = loadFile(userID=3, signatureID=1, drawPlot=False)
newSignatureVector = assemblyFeatureVectorForData(newSignature)
userID = KNN(newSignatureVector, 5)
print(userID)
"""