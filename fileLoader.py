import pandas as pd
import matplotlib.pyplot as plt

from features import *
from preprocessing import normalizeCoordinates

headings = ['X-coordinate',
            'Y-coordinate',
            'timestamp',
            'buttonStatus',
            'azimuth',
            'altitude',
            'pressure']

databaseHeadings = [
    'user',
    'signature',
    'isFake',
    'time',
    'penUps',
    'averageVelocity',
    'xCoordMean',
    'xCoordStd',
    'yCoordMean',
    'yCoordStd',
    'azimuthMean',
    'azimuthStd',
    'altitudeMean',
    'altitudeStd',
    'pressureMean',
    'pressureStd',
    'xVelocityMean',
    'xVelocityStd',
    'yVelocityMean',
    'yVelocityStd'
]

databaseHeadings2 = [
    'user',
    'signature',
    'isFake',
    'time',
    'penUps',
    'averageVelocity',
    'xCoordMean',
    'xCoordStd',
    'yCoordMean',
    'yCoordStd',
    'azimuthMean',
    'azimuthStd',
    'altitudeMean',
    'altitudeStd',
    'pressureMean',
    'pressureStd',
]


def loadFile(userID: int, signatureID: int, drawPlot=False):
    filename = './dane/U' + str(userID) + 'S' + str(signatureID) + '.TXT'
    data = pd.read_csv(filename, names=headings, delim_whitespace=True, skiprows=1)

    data["timestamp"] = data["timestamp"] - data["timestamp"][0]

    data = normalizeCoordinates(data, 1, True)

    if drawPlot:
        plt.plot(data["X-coordinate"], data["Y-coordinate"], 'ko-')
        plt.show()

    return data


def makeSeries(filename: str) -> list:
    data = pd.read_csv(filename, names=headings, delim_whitespace=True, skiprows=1)

    data["timestamp"] = data["timestamp"] - data["timestamp"][0]
    data = normalizeCoordinates(data, 1, True)
    features = assemblyFeatureVectorForData2(data)

    filename = filename[filename.find('/') + 1:]
    filename = filename[: filename.find('.')]
    U = filename.find("U")
    S = filename.find("S")
    user = int(filename[U + 1: S])
    signature = int(filename[S + 1:])
    isFake = signature > 20
    record = [user, signature, isFake]
    record.extend(features)

    return record


