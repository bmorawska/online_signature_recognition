import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def plotGraph(data, userID, whichFeature):
    dataX = []
    for i in range(1, 41):
        dataX.append(i)

    dataY = []
    tempdf = df[(df['user'] == userID)]
    for index, signature in tempdf.iterrows():
        dataY.append(signature[whichFeature])

    colors = []
    for i in range(0, 20):
        colors.append('green')

    for i in range(0, 20):
        colors.append('red')

    plt.scatter(dataX, dataY, c=colors)
    plt.title("Wartości cechy {} dla użytkownika {}".format(whichFeature, userID))
    plt.xlabel("Numer podpisu")
    plt.ylabel("Wartość cechy")
    plt.xticks(np.arange(0, 41, step=2))
    plt.show()



df = pd.read_csv('signatures_db.csv')
df = df.sort_values(by=['user', 'signature'], ascending=True)

plotGraph(df, 33, "pressureMean")