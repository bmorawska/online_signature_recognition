import os

import pandas as pd
from tqdm import tqdm

from fileLoader import *

"""
PL: Uruchom ten plik, żeby wygenerować bazę danych na podstawie podpisów z katalogu ./dane.
EN: Launch this script to generate a database based on signatures in ./dane dir
"""

dir = os.listdir('dane/')
rows = []

for filename in tqdm(dir):
    rows.append(makeSeries('dane/' + filename))

df = pd.DataFrame(rows, columns=databaseHeadings2)
df.to_csv("signatures_db.csv")
