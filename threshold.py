import numpy as np


def calculateThresholdForUser(userIndex, df):
    tempdf = df.copy()
    tempdf = tempdf[(tempdf['user'] == userIndex) & (tempdf['signature'] < 21)]
    testdf = tempdf.sample(n=5)

    avgDistances = []
    minDistances = []
    quantiles = []
    for testIndex, signature in testdf.iterrows():
        featureVector = signature[4:]
        minDist = 100.0
        distances = []
        for dfIndex, row in tempdf.iterrows():
            dbVector = row[4:]
            dist = np.linalg.norm(featureVector - dbVector)
            distances.append(dist)
            if dist < minDist and row['signature'] != signature['signature']:
                minDist = dist

        minDistances.append(minDist)
        quantiles.append(np.quantile(distances, 0.25))

    threshold = np.average(quantiles)
    return threshold
