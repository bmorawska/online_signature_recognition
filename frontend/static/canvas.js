function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}


let canvas = document.querySelector('canvas');
canvas.width = 1150;
canvas.height = 350;
let context = canvas.getContext('2d');

//Values from table
let positiontable = document.getElementById("position-table");
let pressureTable = document.getElementById("pressure-table");
let isDownTable = document.getElementById("is-down-table");
let tangentPressureTable = document.getElementById("tangent-pressure-table");
let tiltTable = document.getElementById("tilt-table");

let penDown = false;
let lastPoint;
let signatureData = [];

let button = document.getElementById('check-signature');
button.onclick =  function(){
    const jsonString = JSON.stringify(signatureData);
    const http = new XMLHttpRequest();
    const url = "http://localhost:5000";
    http.open("POST", url);
    http.send(jsonString)
};

canvas.addEventListener('pointermove', function(event){
    const penPos = getMousePos(canvas, event);
    const penPressure = event.pressure;
    const tangentialPressure = event.tangentialPressure;
    const tiltX = event.tiltX;
    const tiltY = event.tiltY;

    positiontable.innerHTML =  penPos.x + ', ' + penPos.y;
    pressureTable.innerHTML = penPressure;
    isDownTable.innerHTML = penDown ? "Tak" : "Nie";
    tangentPressureTable.innerHTML = tangentialPressure;
    tiltTable.innerHTML = tiltX + ", " + tiltY;

    if (penDown) {
        context.beginPath();
        context.moveTo(lastPoint.x, lastPoint.y);
        context.lineTo(penPos.x, penPos.y);
        context.stroke();
        context.lineWidth = penPressure * 6;
        lastPoint = penPos;



    const singleSignatureSample = {
        "X-coordinate": penPos.x,
        "Y-coordinate": penPos.y,
        "timestamp": Date.now(),
        "buttonStatus": penDown,
        "tiltX": tiltX,
        "tiltY":tiltY,
        "tangentialPressure": tangentialPressure,
        "pressure": penPressure
    };

    signatureData.push(singleSignatureSample)
}
});

canvas.addEventListener('pointerdown', function(event){
    penDown = true;
    lastPoint = getMousePos(canvas, event)
});

canvas.addEventListener('pointerup', function(event){
    penDown = false;
});