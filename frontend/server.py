from flask import Flask, render_template, request, jsonify
import logging

logging.basicConfig(level=logging.DEBUG)
app = Flask(__name__)


@app.route('/' , methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')

    if request.method == 'POST':
        data = request.form.to_dict(flat=False)
        app.logger.info(jsonify(data))
        return "Tu wyskoczy komunikat o twoim podpisie"


if __name__ == "__main__":
    app.run(debug=True)