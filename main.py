import pandas as pd

from KNN import KNN2
from threshold import *

df = pd.read_csv('signatures_db.csv')
dfNoFakes = df[(df["isFake"] == False)]

## Do generowania wartości progowych podpisów
"""
thresholds = np.zeros(40)

for i in range(1, 41):
    thresholds[i - 1] = calculateThresholdForUser(i, df)

np.savetxt('thresholds.csv', thresholds, delimiter=',')
"""
thresholds = np.loadtxt('thresholds.csv', delimiter=',')
print(thresholds)

df = df.sort_values(by=['user', 'signature'], ascending=True)

outputColumns = ['TP', 'TN', 'FP', 'FN']
outputdf = pd.DataFrame(columns=outputColumns)

totalTruePositive = 0
totalTrueNegative = 0
totalFalsePositive = 0
totalFalseNegative = 0
for i in range(1, 41):
    userdfNoFakes = df[(df['user'] == i) & (df["isFake"] == False)]
    userdfFakes = df[(df['user'] == i) & (df["isFake"] == True)]
    truePositives = 0
    trueNegatives = 0
    for index, signature in userdfNoFakes.iterrows():
        vector = signature[4:]
        userThresholdIndex = i - 1
        idx, isSignatureValid = KNN2(vector, dfNoFakes, thresholds[userThresholdIndex], 3)

        if signature["user"] == idx and isSignatureValid:
            truePositives += 1
            totalTruePositive += 1
        else:
            trueNegatives += 1
            totalTrueNegative += 1

    print("TP / TN dla użytkownika {}: {}, {}".format(i, truePositives, trueNegatives))

    falsePositives = 0
    falseNegatives = 0
    for index, signature in userdfFakes.iterrows():
        vector = signature[4:]
        userThresholdIndex = i - 1
        idx, isSignatureValid = KNN2(vector, dfNoFakes, thresholds[userThresholdIndex], 3)

        if signature["user"] == idx and isSignatureValid:
            falsePositives += 1
            totalFalsePositive += 1
        else:
            falseNegatives += 1
            totalFalseNegative += 1

    print("FP / FN dla użytkownika {}: {}, {}".format(i, falsePositives, falseNegatives))
    userRow = {'TP': truePositives, 'TN': trueNegatives, 'FP': falsePositives, 'FN': falseNegatives}
    outputdf = outputdf.append(userRow, ignore_index=True)
    print(outputdf)

print("Całkowite statystyki:")
print("TP: {}, TN: {}, FP: {}, FN: {}".format(totalTruePositive, totalTrueNegative, totalFalsePositive, totalFalseNegative))
outputdf.to_csv('wyniki.csv', index=False)
